create table if not exists stock (
id int primary key auto_increment,
stockTicker varchar (30),
price double,
volume int,
buyOrSell  varchar(30),
statusCodecity int
);
insert into stock (id,stockTicker,price,volume,buyOrSell,statusCodeCity) values(1,'Citi',100,10,'buy',0);
insert into stock (id,stockTicker,price,volume,buyOrSell,statusCodeCity) values(2,'APPLE',500,15,'sell',1);
insert into stock (id,stockTicker,price,volume,buyOrSell,statusCodeCity) values(3,'NFLX',600,100,'buy',2);
insert into stock (id,stockTicker,price,volume,buyOrSell,statusCodeCity) values(4,'KFC',50,60,'sell',1);
insert into stock (id,stockTicker,price,volume,buyOrSell,statusCodeCity) values(5,'JPM',400,500,'buy',2);
